import { Injectable } from '@angular/core';
import { Word } from './Entities';

@Injectable({
  providedIn: 'root'
})
export class WordsService {

  constructor() { }

  Aimer: Word = new Word("Aimer", ["Maire", "Marie, Ramie"]);
  Ironique: Word = new Word("Ironique", ["Onirique"]);
  Chien: Word =  new Word("Chien", ["Cheni", "Chine", "Niche"]);
  Argent: Word = new Word("Argent", ["Ganter", "Garent", "Gerant", "Greant", "Grenat", "Ragent", "Regnat"]);
  Parisien: Word = new Word("Parisien", ["Aspirine"]);
  Police: Word = new Word("Police", ["Picole"]);
  Plage: Word = new Word("Plage", ["Pagel"]);
  Mot: Word = new Word("Mot", ["Tom"]);
  Bancaire: Word = new Word("Bancaire", ["Carabine"]);
  Nationalisé : Word = new Word("Nationalisé", ["Aliénation"]);
  Obturation: Word = new Word("Obturation", ["Aboutiront"]);
  Sautiller: Word = new Word("Sautiller", ["Tailleurs"]);
  Calvaire: Word = new Word("Calvaire", ["Cavalier", "Calvaire"]);
  Harpe : Word = new Word("Harpe", ["Phare"]);
  Fake: Word = new Word("Fake", ["Kafe"]);
 
  getWordList(): Word[] {
    let wordList: Word[] = [this.Aimer, this.Ironique, this.Chien, 
      this.Argent, this.Parisien, this.Police, this.Plage, this.Mot,
      this.Bancaire, this.Nationalisé, this.Obturation, this.Sautiller, 
      this.Calvaire, this.Harpe, this.Fake];
    return wordList;
  }
  
}
