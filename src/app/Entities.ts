export class Letter {
  occurence : number
  letter :  string

  constructor(occ : number, letter : string) {
    this.occurence = occ;
    this.letter = letter;
  }
}

export class Word {
  word : string
  annagrammes : string[]

  constructor(word: string, annagrammes: string[]) {
    this.word = word;
    this.annagrammes = annagrammes;
  }
}