import { Component, OnInit } from '@angular/core';
import { Letter, Word } from '../Entities';
import { WordsService } from '../words.service';

@Component({
  selector: 'app-central-window',
  templateUrl: './central-window.component.html',
  styleUrls: ['./central-window.component.css']
})
export class CentralWindowComponent implements OnInit {

  constructor(private wordsService: WordsService) { }

  displayRep : boolean = false;
  response : string = "";

  toggleBtn : boolean = true;
  
  wordCount : number = 0;
  score : number = 0;

  keyWordToGuess:string = "";
  keyWordToCompare:string = "";

  gameRunning : boolean = true;
  
  wordList: Word[] = this.wordsService.getWordList();

  wordToGuessObject: Word = this.wordList[this.wordCount];
  allAnagramms:string[] = [];
  wordToGuess: string = this.wordList[this.wordCount].word;
  wordToCompare : string = '';

  // Fonction lancée par le bouton principal :

  sendResult() {
    let toCompare = this.keyGenrator(this.wordToCompare);
    console.log(this.wordToCompare);
    let toGuess = this.keyGenrator(this.wordToGuess);
    let isValide = this.keyComparator(toGuess, toCompare);
    console.log(isValide);
    let isEqual = this.areEquals(this.wordToGuess.toLowerCase(), this.wordToCompare.toLowerCase());
    console.log(isEqual);
    let exists = this.checkExistence(this.wordToCompare.toLowerCase(), this.wordToGuessObject.annagrammes)
    console.log(exists);
    this.responseGenerator(isEqual, isValide, exists);
    this.allAnagramms = this.wordToGuessObject.annagrammes;
    this.wordToCompare = "";
    this.toggleBtn = !this.toggleBtn;
  }
  // 
  next() {
    this.toggleBtn = !this.toggleBtn;
    if (this.wordCount < 13) {
      this.wordCount++;
    } else {
      this.gameRunning = false;
    }
    this.wordToGuessObject = this.wordList[this.wordCount];
    this.wordToGuess = this.wordList[this.wordCount].word;
  }

  hereWeGoAgain() {
    this.gameRunning = true;
    this.wordToGuess = this.wordList[0].word;
    this.score = 0;
    this.wordCount = 0;
  }

  responseGenerator(isEqual:boolean, isAnAnagramme:boolean, exists:boolean) {
    let response:string = "";
    if (isEqual && exists == false && isAnAnagramme) {
      response = "Effectivement " + this.wordToGuess + " est un anagramme de lui-même... Cependant vous ne gagnez pas de point 🥸 ";
    } else if (isEqual == false && isAnAnagramme && exists == false) {
      response = "Il s'agit techniquement d'un anagramme de " + this.wordToGuess + ", mais le mot n'existe pas... "
    } else if (isEqual == false && isAnAnagramme && exists) {
      this.score++
      response = "Bravo ! " + this.wordToCompare +  " est bien un anagramme de " + this.wordToGuess + " !"
    } else if (isEqual == false && isAnAnagramme == false && exists == false) {
      response = "Il n'y a rien qui va..."
    } else {
      response = "comment vous avez fait pour afficher ça..."
    }
    this.response = response;
  }

  checkExistence(wordToCheck:string, listAnagramms:string[]):boolean{
    console.log(wordToCheck);
    console.log(listAnagramms)
    let arrayForCheck = listAnagramms.map(el => {
      return el.toLowerCase();
    });
    console.log(arrayForCheck);
    console.log(arrayForCheck.includes(wordToCheck));
    return arrayForCheck.includes(wordToCheck);
  }

  // Key generator :
  keyGenrator(word : string): Letter[] {
    let charArray = Array.from(word.toLowerCase());
    let stockLetters : Letter[] = [];

    let i : number = 0;

    charArray.forEach(ltt => {
      if (stockLetters.some(e => e.letter == ltt)) {
        stockLetters.forEach(e => {
          if (e.letter == ltt) {
            e.occurence = e.occurence + 1;
          }
        })
      } else {
        let letter = new Letter(1, ltt);
        stockLetters.push(letter);
      }
    })
    return stockLetters;
  }

  areEquals(word1:string, word2:string):boolean {
    let response:boolean;
    if (word1 === word2) {
      response = true;
    } else {
      response = false;
    }
    return response;
  }

  keyComparator(toGuess: Letter[], toCompare: Letter[]): boolean {
    let count = 0;
    let response : boolean = false;
    toGuess.forEach(x => {
      toCompare.forEach(y => {
        if (x.letter == y.letter && x.occurence == y.occurence){
          count++
        }
      })
    })

    if (count == toGuess.length && count == toCompare.length) {
      response = true;
    } else {
      response = false
    }
    return response;
  }

  ngOnInit() {
    this.wordList = this.wordsService.getWordList();
  }

}
