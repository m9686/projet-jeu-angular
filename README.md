# ProjetJeu

## Fonctionnement : 

J'ai fait un petit jeu qui consiste à deviner un anagramme pour chaque mot dans une liste, avec une interface simple en Tailwind.
- La liste est une liste d'objets de type Word qui possèdent : un mot, et une liste de mots qui sont ses anagrammes connus.
- Le joueur entre un mot qui est tranformé en une clé qui permet de le comparé au mot de la liste  afin de savoir s'il s'agit d'un anagramme de celui-ci.
- Ils ne doivent cependant pas être les mêmes et ils doivent se trouver dans la liste (A défaut de pouvoir parcourir un dictionnaire).
- Une réponse est envoyée qui change en fonction du résultat des différentes comparaisons.
- En-dessous de la réponse une la liste d'anagrammes possibles du mot est affichée.
- Une bonne réponse ajoute un point au score finale.
- Après 13 mots le jeu se termine et vouis propose de recommencer afin d'améliorer votre score.

## Défauts : 

- L'arrêt du jeu est fixé au chiffre 13 plutôt qu'a la fin de la liste. Mon système de base le faisait, mais des modification dans l'ont fait bugger et je n'ai pas eu le temps de le réparer.
- J'aurais aimé faire apparaître, sous la réponse, une comparaison des clés générées.
- Et bien sûr pouvoir vérifier l'existence du mot dans un dictionnaire... mais bon...







